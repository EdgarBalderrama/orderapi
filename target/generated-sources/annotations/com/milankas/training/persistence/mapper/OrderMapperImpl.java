package com.milankas.training.persistence.mapper;

import com.milankas.training.domain.LineItem;
import com.milankas.training.domain.Order;
import com.milankas.training.persistence.entity.LineItemEntity;
import com.milankas.training.persistence.entity.OrderEntity;
import java.util.ArrayList;
import java.util.List;
import javax.annotation.Generated;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Generated(
    value = "org.mapstruct.ap.MappingProcessor",
    date = "2021-07-05T01:52:53-0400",
    comments = "version: 1.4.1.Final, compiler: javac, environment: Java 1.8.0_291 (Oracle Corporation)"
)
@Component
public class OrderMapperImpl implements OrderMapper {

    @Autowired
    private AddressMapper addressMapper;
    @Autowired
    private LineItemMapper lineItemMapper;

    @Override
    public Order toOrder(OrderEntity orderEntity) {
        if ( orderEntity == null ) {
            return null;
        }

        Order order = new Order();

        order.setOrderId( orderEntity.getIdOrder() );
        order.setCustomerName( orderEntity.getCustomerName() );
        order.setLineItems( lineItemEntityListToLineItemList( orderEntity.getLineItemEntities() ) );
        order.setEmailAddress( orderEntity.getEmailAddress() );
        order.setShippingAddress( addressMapper.toAddress( orderEntity.getAddressEntity() ) );

        return order;
    }

    @Override
    public List<Order> toOrders(List<OrderEntity> orderEntities) {
        if ( orderEntities == null ) {
            return null;
        }

        List<Order> list = new ArrayList<Order>( orderEntities.size() );
        for ( OrderEntity orderEntity : orderEntities ) {
            list.add( toOrder( orderEntity ) );
        }

        return list;
    }

    @Override
    public OrderEntity toOrderEntity(Order order) {
        if ( order == null ) {
            return null;
        }

        OrderEntity orderEntity = new OrderEntity();

        orderEntity.setIdOrder( order.getOrderId() );
        orderEntity.setCustomerName( order.getCustomerName() );
        orderEntity.setLineItemEntities( lineItemListToLineItemEntityList( order.getLineItems() ) );
        orderEntity.setEmailAddress( order.getEmailAddress() );
        orderEntity.setAddressEntity( addressMapper.toAddressEntity( order.getShippingAddress() ) );

        return orderEntity;
    }

    protected List<LineItem> lineItemEntityListToLineItemList(List<LineItemEntity> list) {
        if ( list == null ) {
            return null;
        }

        List<LineItem> list1 = new ArrayList<LineItem>( list.size() );
        for ( LineItemEntity lineItemEntity : list ) {
            list1.add( lineItemMapper.toLineItem( lineItemEntity ) );
        }

        return list1;
    }

    protected List<LineItemEntity> lineItemListToLineItemEntityList(List<LineItem> list) {
        if ( list == null ) {
            return null;
        }

        List<LineItemEntity> list1 = new ArrayList<LineItemEntity>( list.size() );
        for ( LineItem lineItem : list ) {
            list1.add( lineItemMapper.toLineItemEntity( lineItem ) );
        }

        return list1;
    }
}
