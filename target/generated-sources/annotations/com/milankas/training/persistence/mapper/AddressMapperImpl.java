package com.milankas.training.persistence.mapper;

import com.milankas.training.domain.Address;
import com.milankas.training.persistence.entity.AddressEntity;
import javax.annotation.Generated;
import org.springframework.stereotype.Component;

@Generated(
    value = "org.mapstruct.ap.MappingProcessor",
    date = "2021-07-05T01:52:54-0400",
    comments = "version: 1.4.1.Final, compiler: javac, environment: Java 1.8.0_291 (Oracle Corporation)"
)
@Component
public class AddressMapperImpl implements AddressMapper {

    @Override
    public Address toAddress(AddressEntity addressEntity) {
        if ( addressEntity == null ) {
            return null;
        }

        Address address = new Address();

        address.setAddressId( addressEntity.getIdAddress() );
        address.setAddressLine1( addressEntity.getAddressLine1() );
        address.setAddressLine2( addressEntity.getAddressLine2() );
        address.setContactName( addressEntity.getContactName() );
        address.setContactPhoneNo( addressEntity.getContactPhoneNo() );
        address.setState( addressEntity.getState() );
        address.setCity( addressEntity.getCity() );
        if ( addressEntity.getZipCode() != null ) {
            address.setZipCode( Integer.parseInt( addressEntity.getZipCode() ) );
        }
        address.setCountryCode( addressEntity.getCountryCode() );

        return address;
    }

    @Override
    public AddressEntity toAddressEntity(Address address) {
        if ( address == null ) {
            return null;
        }

        AddressEntity addressEntity = new AddressEntity();

        addressEntity.setIdAddress( address.getAddressId() );
        addressEntity.setAddressLine1( address.getAddressLine1() );
        addressEntity.setAddressLine2( address.getAddressLine2() );
        addressEntity.setContactName( address.getContactName() );
        addressEntity.setContactPhoneNo( address.getContactPhoneNo() );
        addressEntity.setState( address.getState() );
        addressEntity.setCity( address.getCity() );
        if ( address.getZipCode() != null ) {
            addressEntity.setZipCode( String.valueOf( address.getZipCode() ) );
        }
        addressEntity.setCountryCode( address.getCountryCode() );

        return addressEntity;
    }
}
