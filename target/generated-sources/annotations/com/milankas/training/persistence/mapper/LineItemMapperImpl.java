package com.milankas.training.persistence.mapper;

import com.milankas.training.domain.LineItem;
import com.milankas.training.persistence.entity.LineItemEntity;
import javax.annotation.Generated;
import org.springframework.stereotype.Component;

@Generated(
    value = "org.mapstruct.ap.MappingProcessor",
    date = "2021-07-05T01:52:54-0400",
    comments = "version: 1.4.1.Final, compiler: javac, environment: Java 1.8.0_291 (Oracle Corporation)"
)
@Component
public class LineItemMapperImpl implements LineItemMapper {

    @Override
    public LineItem toLineItem(LineItemEntity lineItemEntity) {
        if ( lineItemEntity == null ) {
            return null;
        }

        LineItem lineItem = new LineItem();

        lineItem.setLineItemId( lineItemEntity.getIdLineItem() );
        lineItem.setProductId( lineItemEntity.getIdProduct() );
        lineItem.setQuantity( lineItemEntity.getQuantity() );

        return lineItem;
    }

    @Override
    public LineItemEntity toLineItemEntity(LineItem lineItem) {
        if ( lineItem == null ) {
            return null;
        }

        LineItemEntity lineItemEntity = new LineItemEntity();

        lineItemEntity.setIdLineItem( lineItem.getLineItemId() );
        lineItemEntity.setIdProduct( lineItem.getProductId() );
        lineItemEntity.setQuantity( lineItem.getQuantity() );

        return lineItemEntity;
    }
}
