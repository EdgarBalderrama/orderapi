package com.milankas.training.web.controller;

import com.milankas.training.domain.Order;
import com.milankas.training.domain.repository.OrderRepository;
import com.milankas.training.domain.service.OrderService;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.UUID;

@RunWith(MockitoJUnitRunner.class)
class OrderControllerTest {
//
//    OrderRepository repository = Mockito.mock(OrderRepository.class);
//    RabbitTemplate template = Mockito.mock(RabbitTemplate.class);

    OrderService orderService = Mockito.mock(OrderService.class);

    @Autowired
    OrderController controller = new OrderController(orderService);

    Order order1 = new Order(UUID.fromString("123e4567-e89b-12d3-a456-426614174000"), "Edgar", null, "edgar@gmail.com", null);
    Order order2 = new Order(UUID.randomUUID(), "Rubén", null, "ruben@gmail.com", null);
    Order order3 = new Order(UUID.fromString("123e4567-e89b-12d3-a456-426614174000"), "Rubén", null, "ruben@gmail.com", null);
    Order order4 = new Order(UUID.randomUUID(), "Edgar", null, "edgarNew@gmail.com", null);
    Order order5 = new Order(UUID.randomUUID(), "Pochi", null, "edgar@gmail.com", null);

    @BeforeEach
    public void beforeEach(){
        Mockito.when(orderService.getOrderForRabbitMessage(UUID.fromString("123e4567-e89b-12d3-a456-426614174000"))).thenReturn(order1);
        Mockito.when(orderService.save(order3)).thenReturn(order3);
        Mockito.when(orderService.getOrder(UUID.fromString("123e4567-e89b-12d3-a456-426614174000"))).thenReturn(order3);
        //Mockito.when(orderService.update(order3)).thenReturn(order3);
    }

    @Test
    void getOrder() {
        Assertions.assertEquals(order1 , controller.getOrder(UUID.fromString("123e4567-e89b-12d3-a456-426614174000")));
    }

    @Test
    void getByParams() {
    }

    @Test
    void save() {
        Assertions.assertEquals(order3, controller.save(order3));
    }

    @Test
    void delete() {
    }

    @Test
    void update() {
        Assertions.assertEquals(order3, controller.update(UUID.fromString("123e4567-e89b-12d3-a456-426614174000"), order3));
    }
}