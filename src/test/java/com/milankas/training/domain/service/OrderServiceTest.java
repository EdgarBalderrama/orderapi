//package com.milankas.training.domain.service;
//
//import com.milankas.training.domain.Address;
//import com.milankas.training.domain.LineItem;
//import com.milankas.training.domain.Order;
//import com.milankas.training.domain.repository.OrderRepository;
//import org.aspectj.weaver.World;
//import org.junit.jupiter.api.Assertions;
//import org.junit.jupiter.api.BeforeEach;
//import org.junit.jupiter.api.Test;
//import org.junit.runner.RunWith;
//import org.mockito.Mock;
//import org.mockito.Mockito;
//import org.mockito.junit.MockitoJUnitRunner;
//import org.springframework.amqp.rabbit.core.RabbitTemplate;
//import org.springframework.beans.factory.annotation.Autowired;
//
//import java.util.ArrayList;
//import java.util.List;
//import java.util.UUID;
//
//@RunWith(MockitoJUnitRunner.class)
//class OrderServiceTest {
//
//    OrderRepository repository = Mockito.mock(OrderRepository.class);
//    RabbitTemplate template = Mockito.mock(RabbitTemplate.class);
//
//    @Autowired
//    OrderService service = new OrderService(repository, template);
//
//    Address address = new Address();
//    LineItem lineItem1 = new LineItem();
//    LineItem lineItem2 = new LineItem();
//
//    List<LineItem> lineItems = new ArrayList<>();
//
//    Order order1 = new Order(UUID.randomUUID(), "Edgar", lineItems, "edgar@gmail.com", address);
//    Order order2 = new Order(UUID.randomUUID(), "Rubén", null, "ruben@gmail.com", null);
//    Order order3 = new Order(UUID.fromString("123e4567-e89b-12d3-a456-426614174000"), "Rubén", null, "ruben@gmail.com", null);
//    Order order4 = new Order(UUID.randomUUID(), "Edgar", null, "edgarNew@gmail.com", null);
//    Order order5 = new Order(UUID.randomUUID(), "Pochi", null, "edgar@gmail.com", null);
//
//
//    @BeforeEach
//    public void beforeEach(){
//        address.setAddressId(UUID.randomUUID());
//        address.setAddressLine1("Av. Gimenez");
//        address.setAddressLine2("C. Chavez");
//        address.setContactName("Edgar");
//        address.setContactPhoneNo("+59176991234");
//        address.setCity("Cochabamba");
//        address.setState("Cercado");
//        address.setZipCode(5234);
//        address.setCountryCode("BO");
//
//        lineItem1.setProductId(UUID.randomUUID());
//        lineItem1.setLineItemId(UUID.randomUUID());
//        lineItem1.setQuantity(5);
//
//        lineItem2.setProductId(UUID.randomUUID());
//        lineItem2.setLineItemId(UUID.randomUUID());
//        lineItem2.setQuantity(10);
//
//        lineItems.add(lineItem1);
//        lineItems.add(lineItem2);
//
//        List<Order> allOrders = new ArrayList<>();
//        allOrders.add(order1);
//        allOrders.add(order2);
//        allOrders.add(order3);
//        allOrders.add(order4);
//        allOrders.add(order5);
//
//        List<Order> customerNameList = new ArrayList<>();
//        customerNameList.add(order1);
//        customerNameList.add(order4);
//
//        List<Order> emailList = new ArrayList<>();
//        emailList.add(order1);
//        emailList.add(order5);
//
//        List<Order> nameAndEmailList = new ArrayList<>();
//        nameAndEmailList.add(order1);
//
//        Mockito.when(repository.getAll()).thenReturn(allOrders);
//        Mockito.when(repository.getOrder(UUID.fromString("123e4567-e89b-12d3-a456-426614174000"))).thenReturn(java.util.Optional.ofNullable(order3));
//        Mockito.when(repository.getByCustomerName("Edgar")).thenReturn(customerNameList);
//        Mockito.when(repository.getByEmailAddress("edgar@gmail.com")).thenReturn(emailList);
//        Mockito.when(repository.getByNameAndEmail("Edgar", "edgar@gmail.com")).thenReturn(nameAndEmailList);
//        Mockito.when(repository.save(order3)).thenReturn(order3);
//        //Mockito.doThrow(new Exception()).doNothing().when(repository).delete(UUID.fromString("123e4567-e89b-12d3-a456-426614174000"));
//    }
//
//    @Test
//    void getAll() {
//        List<Order> allOrders = new ArrayList<>();
//        allOrders.add(order1);
//        allOrders.add(order2);
//        allOrders.add(order3);
//        allOrders.add(order4);
//        allOrders.add(order5);
//        Assertions.assertEquals(allOrders, service.getAll());
//    }
//
//    @Test
//    void getOrderForRabbitMessage() {
//        Assertions.assertEquals(order3 , service.getOrderForRabbitMessage(UUID.fromString("123e4567-e89b-12d3-a456-426614174000")));
//    }
//
//    @Test
//    void getOrder() {
//        Assertions.assertEquals(order3 , service.getOrder(UUID.fromString("123e4567-e89b-12d3-a456-426614174000")));
//    }
//
//    @Test
//    void getByCustomerName() {
//        List<Order> testName = new ArrayList<>();
//        testName.add(order1);
//        testName.add(order4);
//        Assertions.assertEquals(testName , service.getByCustomerName("Edgar"));
//    }
//
//    @Test
//    void getByEmailAddress() {
//        List<Order> testEmail = new ArrayList<>();
//        testEmail.add(order1);
//        testEmail.add(order5);
//        Assertions.assertEquals(testEmail , service.getByEmailAddress("edgar@gmail.com"));
//    }
//
//    @Test
//    void getByNameAndEmail() {
//        List<Order> testNameAndEmail = new ArrayList<>();
//        testNameAndEmail.add(order1);
//        Assertions.assertEquals(testNameAndEmail , service.getByNameAndEmail("Edgar", "edgar@gmail.com"));
//    }
//
//    @Test
//    void save() {
//        Assertions.assertEquals(order3, service.save(order3));
//    }
//
//    @Test
//    void update() {
//        //Assertions.assertEquals(order3, service.update(order3));
//    }
//
//    @Test
//    void delete() {
//    }
//}