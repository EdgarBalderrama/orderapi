package com.milankas.training.persistence.mapper;

import com.milankas.training.domain.Order;
import com.milankas.training.persistence.entity.OrderEntity;
import org.mapstruct.InheritInverseConfiguration;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.Mappings;

import java.util.List;

@Mapper(componentModel = "spring", uses = {AddressMapper.class, LineItemMapper.class})
public interface OrderMapper {

    @Mappings({
            @Mapping(source = "idOrder", target = "orderId"),
            @Mapping(source = "customerName", target = "customerName"),
            @Mapping(source = "lineItemEntities", target = "lineItems"),
            @Mapping(source = "emailAddress", target = "emailAddress"),
            @Mapping(source = "addressEntity", target = "shippingAddress"),
    })
    Order toOrder(OrderEntity orderEntity);
    List<Order> toOrders(List<OrderEntity> orderEntities);

    @InheritInverseConfiguration
    OrderEntity toOrderEntity(Order order);
}
