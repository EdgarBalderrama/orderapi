package com.milankas.training.persistence.mapper;

import com.milankas.training.domain.LineItem;
import com.milankas.training.persistence.entity.LineItemEntity;
import org.mapstruct.InheritInverseConfiguration;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.Mappings;

import java.util.List;

@Mapper(componentModel = "spring", uses = {OrderMapper.class})
public interface LineItemMapper {

    @Mappings({
            @Mapping(source = "idLineItem", target = "lineItemId"),
            @Mapping(source = "idProduct", target = "productId"),
    })
    LineItem toLineItem(LineItemEntity lineItemEntity);

    @InheritInverseConfiguration
    LineItemEntity toLineItemEntity(LineItem lineItem);
}
