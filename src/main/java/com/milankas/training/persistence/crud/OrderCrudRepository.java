package com.milankas.training.persistence.crud;

import com.milankas.training.persistence.entity.OrderEntity;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.UUID;

public interface OrderCrudRepository extends JpaRepository<OrderEntity, UUID> { }
