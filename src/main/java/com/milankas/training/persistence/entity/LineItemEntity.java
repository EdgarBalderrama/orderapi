package com.milankas.training.persistence.entity;


import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.util.UUID;

@Entity
@Table(name = "line_items")
@Getter
@Setter
public class LineItemEntity {

    @Id
    @GeneratedValue
    @Column(name = "id_line_item")
    private UUID idLineItem;

    @Column(name = "product_id")
    private UUID idProduct;

    private Integer quantity;
}
