package com.milankas.training.persistence.entity;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.util.List;
import java.util.UUID;

@Entity
@Table(name = "orders")
@Getter
@Setter
public class OrderEntity {
    @Id
    @GeneratedValue
    @Column(name = "id_order")
    private UUID idOrder;

    @Column(name = "customer_name")
    private String customerName;

    @OneToMany(cascade = CascadeType.ALL)
    private List<LineItemEntity> lineItemEntities;

    @Column(name = "email_address")
    private String emailAddress;

    @ManyToOne(cascade = CascadeType.ALL)
    private AddressEntity addressEntity;
}
