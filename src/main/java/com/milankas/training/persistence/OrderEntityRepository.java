package com.milankas.training.persistence;

import com.milankas.training.domain.Order;
import com.milankas.training.domain.repository.OrderRepository;
import com.milankas.training.persistence.crud.OrderCrudRepository;
import com.milankas.training.persistence.entity.OrderEntity;
import com.milankas.training.persistence.mapper.OrderMapper;
import org.springframework.data.domain.Example;
import org.springframework.data.domain.ExampleMatcher;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;
import java.util.UUID;

import static org.springframework.data.domain.ExampleMatcher.GenericPropertyMatchers.contains;

@Repository
public class OrderEntityRepository implements OrderRepository {

    private final OrderCrudRepository orderCrudRepository;
    private final OrderMapper orderMapper;

    public OrderEntityRepository(OrderCrudRepository orderCrudRepository, OrderMapper orderMapper) {
        this.orderCrudRepository = orderCrudRepository;
        this.orderMapper = orderMapper;
    }

    @Override
    public Optional<Order> getOrder(UUID idOrder) {
        return orderCrudRepository.findById(idOrder).map(orderMapper::toOrder);
    }

    @Override
    public List<Order> findAll(Order order) {
        OrderEntity orderEntity = orderMapper.toOrderEntity(order);
        ExampleMatcher matcher = ExampleMatcher.matching().withIgnoreCase();

        Example<OrderEntity> orderEntityExample = Example.of(orderEntity, matcher);
        List<OrderEntity> orders = orderCrudRepository.findAll(orderEntityExample);

        return orderMapper.toOrders(orders);
    }

    @Override
    public Order save(Order order) {
        OrderEntity orderEntity = orderMapper.toOrderEntity(order);
        return orderMapper.toOrder(orderCrudRepository.save(orderEntity));
    }

    @Override
    public void delete(UUID idOrder) {
        orderCrudRepository.deleteById(idOrder);
    }
}
