package com.milankas.training.web.controller;

import com.milankas.training.domain.Order;
import com.milankas.training.domain.service.OrderService;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;
import java.util.UUID;

@RestController
@RequestMapping("/orders")
public class OrderController {
    private final OrderService orderService;

    public OrderController(OrderService orderService) {
        this.orderService = orderService;
    }

    @GetMapping("/{id}")
    public Order getOrder(@PathVariable("id") UUID orderId) {
        return orderService.getOrderForRabbitMessage(orderId);
    }

    @GetMapping
    public List<Order> getByParams(@RequestParam(value = "customerName", required = false) String customerName,
                                   @RequestParam(value = "email", required = false) String email) {
        return orderService.findBy(customerName, email);
    }

    @PostMapping()
    public Order save(@Valid @RequestBody Order order) {
        return orderService.save(order);
    }

    @DeleteMapping("/{id}")
    public void delete(@PathVariable("id") UUID orderId) {
        orderService.delete(orderId);
    }

    @PatchMapping("/{id}")
    public Order update(@PathVariable("id") UUID id, @Valid @RequestBody Order userOrder) {
        return orderService.update(id, userOrder);
    }
}
