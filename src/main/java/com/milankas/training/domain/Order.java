package com.milankas.training.domain;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.validation.Valid;
import javax.validation.constraints.*;
import java.util.List;
import java.util.UUID;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class Order {

    private UUID orderId;

    @Size(min = 2, max = 60)
    private String customerName;

    @Valid
    private List<LineItem> lineItems;

    @Email
    private String emailAddress;

    @Valid
    private Address shippingAddress;
}
