package com.milankas.training.domain;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Getter;
import lombok.Setter;

import javax.validation.constraints.Positive;
import java.util.UUID;

@Getter
@Setter
public class LineItem {

    @JsonProperty(access = JsonProperty.Access.WRITE_ONLY)
    private UUID lineItemId;

    private UUID productId;

    @Positive
    private Integer quantity;
}
