package com.milankas.training.domain.service;

import com.milankas.training.domain.Order;
import com.milankas.training.domain.exceptionhandler.apiexceptions.NotFoundException;
import com.milankas.training.domain.messagebroker.MessageBroker;
import com.milankas.training.domain.repository.OrderRepository;
import com.milankas.training.domain.utils.MyBeansUtil;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;
import java.util.UUID;

@Service
public class OrderService {

    private final OrderRepository orderRepository;
    private final MessageBroker messageBroker;

    public OrderService(OrderRepository orderRepository, MessageBroker messageBroker) {
        this.orderRepository = orderRepository;
        this.messageBroker = messageBroker;
    }

    public Order getOrder(UUID orderId){
        Optional<Order> order = orderRepository.getOrder(orderId);
        if (!order.isPresent()){
            throw new NotFoundException("Order does not exists");
        }
        return order.get();
    }

    public Order getOrderForRabbitMessage(UUID orderId){
        Optional<Order> order = orderRepository.getOrder(orderId);
        if (!order.isPresent()){
            throw new NotFoundException("Order does not exists");
        }
        messageBroker.send("Status Code: 200 ok. HTTP Method: Get. Message: Get order by ID: " + orderId);
        return order.get();
    }

    public List<Order> findBy(String name, String email){
        Order order = new Order();
        order.setCustomerName(name);
        order.setEmailAddress(email);

        messageBroker.send("Status Code: 200 ok. HTTP Method: Get. Message: Find orders");
        return orderRepository.findAll(order);
    }

    public Order save(Order order){
        messageBroker.send("Status Code: 200 ok. HTTP Method: Post. Message: New order saved");
        return orderRepository.save(order);
    }

    public Order update(UUID id, Order userOrder){
        messageBroker.send("Status Code: 200 ok. HTTP Method: Patch. Message: Order updated");

        Order order = getOrder(id);
        MyBeansUtil<Order> cloner = new MyBeansUtil<Order>();
        order = cloner.copyNonNullProperties(order, userOrder);

        return orderRepository.save(order);
    }

    public void delete(UUID orderId){
        Order order = getOrder(orderId);
        messageBroker.send("Status Code: 200 ok. HTTP Method: Delete. Message: Order deleted");
        orderRepository.delete(order.getOrderId());
    }
}
