package com.milankas.training.domain.repository;

import com.milankas.training.domain.Order;

import java.util.List;
import java.util.Optional;
import java.util.UUID;

public interface OrderRepository {

    List<Order> findAll(Order order);
    Optional<Order> getOrder(UUID orderId);
    Order save(Order order);
    void delete(UUID orderId);
}
