package com.milankas.training;

import io.swagger.v3.oas.annotations.OpenAPIDefinition;
import io.swagger.v3.oas.annotations.info.Info;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
@OpenAPIDefinition(info = @Info(title = "Order API", version = "1.0.0", description = "Order Microservice"))
public class OrderApiApplication {
    public static void main (String[] args){
        SpringApplication.run(OrderApiApplication.class, args);
    }
}
